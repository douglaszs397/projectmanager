var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('express-session');
var flash = require('express-flash');
moment = require('moment');
var CronJob = require('cron').CronJob;


var mysqlSession = require('express-mysql-session')(session);
const multer = require('multer');


//upload = multer({ dest: 'public/uploads/files/'});

upload = multer({ dest: 'public/uploads/files' })

config = require('./config/config');

console.log("CONFIG",config);

/* config.moneda.usd = 3.72; */

console.log(config);

var app = express();

let sessionStore = new mysqlSession(config.mysqlSession);

app.use(session({
  key: 'session_cookie_name',
  secret: 'session_cookie_secret',
  store: sessionStore,
  resave: false,
  saveUninitialized: false
}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use( session ({
  secret: 'session',
  cookie: {}
}));

app.use(flash());

// Importar Models de models.js y permitir su uso en cualquier componente.
require('./models');

app.use('/', require('./routes/index'));


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});



module.exports = app;