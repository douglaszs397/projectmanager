
module.exports = [

  {
    banco: "BCP",
    nrocuenta: "570-224562",
    cii: "0057025146546",
    moneda: "pen",
    tag: "tag1"
  },

  {
    banco: "BCP",
    nrocuenta: "570-2245620000",
    cii: "0057025146546",
    moneda: "pen",
    tag: "tag2"
  },

  {
    banco: "BCP",
    nrocuenta: "570-224562",
    cii: "0057025146546",
    moneda: "usd",
    tag: "tag2"
  },
 
];
