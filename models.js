const { Sequelize, DataTypes, Op } = require('sequelize'); //Cargamos la Libreria

// 'sgbd://user:password@servidor/bd'
const sequelize = new Sequelize('mysql://root:@db/pm', {
  define: {
    freezeTableName: true
  }
}); 

Models = {};


Models.Root = sequelize.define('root', {
  idRoot: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  email: {
    type: DataTypes.STRING
  },
  password: {
    type: DataTypes.STRING
  },
  estado: {
    type: DataTypes.TINYINT
  }
});


Models.Empresa = sequelize.define('empresa', {
  idEmpresa: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  razonSocialEmpresa: {
    type: DataTypes.STRING
  },
  rucEmpresa: {
    type: DataTypes.CHAR
  },
  emailEmpresa: {
    type: DataTypes.STRING
  },
  importancia: {
    type: DataTypes.TINYINT
  },
  estado: {
    type: DataTypes.TINYINT
  }
});


Models.Proyecto = sequelize.define('proyecto', {
  idProyecto: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  idEmpresa: {
    type: DataTypes.INTEGER,
    model: this.Empresa,
    key: 'idEmpresa'
  },
  nombreProyecto: {
    type: DataTypes.STRING
  },
  estadoProyecto: {
    type: DataTypes.STRING
  },
  costoDia: {
    type: DataTypes.INTEGER
  },
  totalDias: {
    type: DataTypes.INTEGER
  },
  costoTotal: {
    type: DataTypes.INTEGER
  },
  estado: {
    type: DataTypes.TINYINT
  }
});


Models.Requerimiento = sequelize.define('requerimiento', {
  idRequerimiento: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  idProyecto: {
    type: DataTypes.INTEGER,
    model: this.Proyecto,
    key: 'idProyecto'
  },
  nombreRequerimiento: {
    type: DataTypes.STRING
  },
  tiempoRequerimiento: {
    type: DataTypes.INTEGER
  },
  etapaRequerimiento: {
    type: DataTypes.INTEGER
  },
  descripcionRequerimiento: {
    type: DataTypes.TEXT
  },
  comentariosRequerimiento: {
    type: DataTypes.TEXT
  },
  estado: {
    type: DataTypes.TINYINT
  }
});


Models.Compra = sequelize.define('compra', {
  idCompra: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  numeroSerieCompra: {
    type: DataTypes.STRING
  },
  idEmpresaCompra: {
    type: DataTypes.INTEGER,
    model: this.Empresa,
    key: 'idEmpresa'
  },
  idProyecto: {
    type: DataTypes.INTEGER,
    model: this.Proyecto,
    allowNull: true,
    key: 'idProyecto'
  },
  tituloCompra: {
    type: DataTypes.STRING
  },
  numeroFactura: {
    type: DataTypes.STRING
  },
  descripcionCompra: {
    type: DataTypes.TEXT
  },
  adjunto: {
    type: DataTypes.STRING
  },
  neto: {
    type: DataTypes.FLOAT
  },
  igv: {
    type: DataTypes.FLOAT
  },
  totalCompra: {
    type: DataTypes.FLOAT
  },
  fechaCompra: {
    type: DataTypes.DATE
  },
  fechaDeclaracion: {
    type: DataTypes.DATE
  },
  estado: {
    type: DataTypes.TINYINT
  }
});


Models.Venta = sequelize.define('venta', {
  idVenta: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  numeroSerieVenta: {
    type: DataTypes.STRING
  },
  idEmpresaVenta: {
    type: DataTypes.INTEGER,
    model: this.Empresa,
    key: 'idEmpresa'
  },
  idProyecto: {
    type: DataTypes.INTEGER,
    model: this.Proyecto,
    key: 'idProyecto'
  },
  tituloVenta: {
    type: DataTypes.STRING
  },
  numeroFactura: {
    type: DataTypes.STRING
  },
  descripcionVenta: {
    type: DataTypes.TEXT
  },
  adjunto: {
    type: DataTypes.STRING
  },
  neto: {
    type: DataTypes.FLOAT
  },
  igv: {
    type: DataTypes.FLOAT
  },
  totalVenta: {
    type: DataTypes.FLOAT
  },
  fechaVenta: {
    type: DataTypes.DATE
  },
  fechaDeclaracion: {
    type: DataTypes.DATE
  },
  estadoVenta: {
    type: DataTypes.STRING
  },
  estado: {
    type: DataTypes.TINYINT
  }
});


Models.VentaRecurrente = sequelize.define('ventaRecurrente', {
  idVentaRecurrente: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  numeroSerieVentaRecurrente: {
    type: DataTypes.STRING
  },
  idEmpresaVentaRecurrente: {
    type: DataTypes.INTEGER,
    model: this.Empresa,
    key: 'idEmpresa'
  },
  idProyecto: {
    type: DataTypes.INTEGER,
    model: this.Proyecto,
    key: 'idProyecto'
  },
  tituloVentaRecurrente: {
    type: DataTypes.STRING
  },
  numeroFacturaVentaRecurrente: {
    type: DataTypes.STRING
  },
  descripcionVentaRecurrente: {
    type: DataTypes.TEXT
  },
  adjunto: {
    type: DataTypes.STRING
  },
  neto: {
    type: DataTypes.FLOAT
  },
  igv: {
    type: DataTypes.FLOAT
  },
  totalVentaRecurrente: {
    type: DataTypes.FLOAT
  },
  periodoRecurrencia: {
    type: DataTypes.INTEGER
  },
  fechaVentaRecurrente: {
    type: DataTypes.DATE
  },
  fechaDeclaracion: {
    type: DataTypes.DATE
  },
  estado: {
    type: DataTypes.TINYINT
  }
});


Models.Sessions = sequelize.define('sessions', {
  session_id: {
      type: DataTypes.STRING,
      primaryKey: true
  },
  expires: {
      type: DataTypes.INTEGER
  },
  data: {
      type: DataTypes.TEXT
  }
});

require('./models/index');

module.exports = Models, sequelize;
