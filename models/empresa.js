//const Models = require("../models");
/*
Models.Empresa.addHook('beforeDestroy', function(){

});

*/


Models.Empresa.prototype.delete = async function(){
  console.log("DELETING EMPRESA", this.idEmpresa);

  let emp = this ; 

  let filter = { where: { idEmpresa: emp.idEmpresa} };
  let proyectos = await Models.Proyecto.findAll(filter);

  for ( p of proyectos ){
    p.estado = 0;
    await p.save();
  }

  emp.estado = 0;
  //await emp.save();
}

/* console.log(Models.Empresa.prototype); */

//Models.Empresa.delete();

/* async function test(){
  let empresas = await  Models.Empresa.findAll();

  for ( emp of empresas ){
    emp.delete();
  }

}
test(); */