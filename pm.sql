-- MySQL dump 10.13  Distrib 5.7.32, for Linux (x86_64)
--
-- Host: localhost    Database: pm
-- ------------------------------------------------------
-- Server version	5.7.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `compra`
--

DROP TABLE IF EXISTS `compra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compra` (
  `idCompra` int(11) NOT NULL AUTO_INCREMENT,
  `numeroSerieCompra` varchar(50) DEFAULT NULL,
  `idEmpresaCompra` int(11) DEFAULT NULL,
  `idProyecto` int(11) DEFAULT NULL,
  `tituloCompra` varchar(300) DEFAULT NULL,
  `numeroFactura` varchar(45) DEFAULT NULL,
  `descripcionCompra` text,
  `adjunto` varchar(200) DEFAULT NULL,
  `neto` decimal(10,2) DEFAULT NULL,
  `igv` decimal(10,2) DEFAULT NULL,
  `totalCompra` decimal(10,2) DEFAULT NULL,
  `fechaCompra` date DEFAULT NULL,
  `fechaDeclaracion` date DEFAULT NULL,
  `estado` tinyint(4) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`idCompra`),
  KEY `idEmpresa_Compra_FK` (`idEmpresaCompra`),
  KEY `idProyecto_Compra_FK` (`idProyecto`),
  CONSTRAINT `idEmpresa_Compra_FK` FOREIGN KEY (`idEmpresaCompra`) REFERENCES `empresa` (`idEmpresa`),
  CONSTRAINT `idProyecto_Compra_FK` FOREIGN KEY (`idProyecto`) REFERENCES `proyecto` (`idProyecto`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compra`
--

LOCK TABLES `compra` WRITE;
/*!40000 ALTER TABLE `compra` DISABLE KEYS */;
INSERT INTO `compra` VALUES (3,'12548795852111111',1,2,'Compra 1','111111','Compra número 1','17a3ca0f5b091ff3f56ff38b2449e079',126.28,27.72,154.00,'2021-01-01','2021-01-01',1,'2021-01-22 05:18:10','2021-03-09 07:07:09'),(4,'125487958522222222222222',1,2,'Compra 2','2222222222222','Compra número 2','0efa2e075ab1ef320c0d7a70c9eb2f0c',102.50,22.50,125.00,'2020-10-10','2020-12-01',1,'2021-01-22 05:19:24','2021-02-26 01:19:21'),(10,'1254879585233333333',2,1,'Compra 3','33333333','Compra número 3','5ed9fd683735d98f0209d39dadb80b4c',102.50,22.50,125.00,'2020-01-01','2021-01-01',1,'2021-01-22 05:36:19','2021-02-16 18:52:35'),(13,'215487858854444444444',4,1,'Compra 4','4444444444','','a320d711556df3bbf16d338a6221da0c',154.98,34.02,189.00,'2021-10-10','2021-01-26',1,'2021-01-22 05:48:00','2021-02-23 23:43:24'),(19,'1236333333366666666',13,1,'Compra 6','66666666','ESTA ES LA COMPRA 06',NULL,NULL,NULL,123.00,'2021-10-10','2021-07-01',1,'2021-01-24 16:52:37','2021-01-27 00:28:48'),(21,'44887854587000123',1,2,'Compra 7','000123','Esta es la compra 07',NULL,1230.00,270.00,1500.00,'2021-02-25','2021-01-01',1,'2021-01-28 15:13:00','2021-02-26 00:49:40'),(22,'12363333333000123',13,2,'Compra 8','000123','Esta es la compra número 8',NULL,NULL,NULL,1000.00,'2021-01-01','2021-02-01',1,'2021-01-28 15:16:58','2021-01-28 15:16:58'),(24,'21548785885510000',4,2,'Compra 9','510000','Compra número 9',NULL,1230.00,270.00,1500.00,'2021-01-01','2023-01-01',1,'2021-01-29 15:59:41','2021-03-04 23:47:31'),(29,'102265874181010',1,NULL,'Compra 10','1010','Compra 10',NULL,8200.00,1800.00,10000.00,'2021-01-08',NULL,1,'2021-02-09 18:12:24','2021-03-04 17:57:40'),(32,'101010101011111111',17,1,'Compra 11','1111111','Compra número 11',NULL,9111.02,1999.98,11111.00,'2021-01-01',NULL,1,'2021-02-09 22:42:11','2021-03-04 17:24:19'),(34,'1022658741811111111',1,1,'Compra 12','11111111','Compra número 12\r\n',NULL,12300.00,2700.00,15000.00,'2021-02-24',NULL,1,'2021-02-24 17:07:34','2021-03-04 23:47:15');
/*!40000 ALTER TABLE `compra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresa`
--

DROP TABLE IF EXISTS `empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresa` (
  `idEmpresa` int(11) NOT NULL AUTO_INCREMENT,
  `razonSocialEmpresa` varchar(500) DEFAULT NULL,
  `rucEmpresa` char(11) DEFAULT NULL,
  `emailEmpresa` varchar(300) DEFAULT NULL,
  `importancia` tinyint(4) DEFAULT NULL,
  `estado` tinyint(4) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`idEmpresa`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresa`
--

LOCK TABLES `empresa` WRITE;
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;
INSERT INTO `empresa` VALUES (1,'Empresa 1 E.I.R.L.','10226587418','empresa1@gmail.com',1,1,NULL,'2021-03-08 23:38:23'),(2,'Empresa 2','12548795852','empresa2@gmail.com',1,1,NULL,'2021-03-08 23:38:23'),(3,'Empresa 03','48758859588','empresa03@gmail.com',1,1,'2021-01-19 21:39:14','2021-03-08 23:38:23'),(4,'Empresa 04','21548785885','empresa04@gmail.com',0,1,'2021-01-22 04:08:34','2021-03-08 23:38:23'),(12,'Empresa 05','12345678901','empresa05@gmail.com',1,1,'2021-01-25 01:08:44','2021-03-08 23:38:23'),(13,'Empresa 06','12363333333','empresa06@gmail.com',0,1,'2021-01-26 05:00:40','2021-03-08 23:38:23'),(14,'Empresa 07','44887854587','empresa07@gmail.com',0,1,'2021-01-26 05:32:35','2021-03-08 23:38:23'),(15,'Empresa 08','88888888888','empresa08@hotmail.com',0,1,'2021-02-07 05:11:58','2021-03-08 23:38:23'),(16,'Empresa 09','99999999999','empresa09@gmail.com',0,1,'2021-02-08 07:14:16','2021-03-08 23:38:23'),(17,'Empresa 10','10101010101','empresa10@gmail.com',0,1,'2021-02-08 07:16:00','2021-03-08 23:38:23'),(24,'Empresa 0','00000000000','empresa0@gmail.com',0,1,'2021-02-09 17:08:44','2021-03-08 06:18:57'),(26,'Melomanía','10739049259','jessicaandrea2316@gmail.com',1,1,'2021-03-08 04:48:14','2021-03-08 23:38:23'),(27,'Empresa 11','11111111111','empresa11@gmail.com',0,1,'2021-03-08 06:19:10','2021-03-08 06:22:11');
/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proyecto`
--

DROP TABLE IF EXISTS `proyecto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proyecto` (
  `idProyecto` int(11) NOT NULL AUTO_INCREMENT,
  `idEmpresa` int(11) DEFAULT NULL,
  `nombreProyecto` varchar(500) DEFAULT NULL,
  `estadoProyecto` varchar(10) DEFAULT NULL,
  `costoDia` int(11) DEFAULT NULL,
  `totalDias` int(11) DEFAULT NULL,
  `costoTotal` int(11) DEFAULT NULL,
  `estado` tinyint(4) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`idProyecto`),
  KEY `idEmpresa_Proyecto_FK` (`idEmpresa`),
  CONSTRAINT `idEmpresa_Proyecto_FK` FOREIGN KEY (`idEmpresa`) REFERENCES `empresa` (`idEmpresa`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proyecto`
--

LOCK TABLES `proyecto` WRITE;
/*!40000 ALTER TABLE `proyecto` DISABLE KEYS */;
INSERT INTO `proyecto` VALUES (1,1,'Proyecto 1','Aceptado',10,52,520,1,NULL,'2021-03-09 07:14:48'),(2,2,'Proyecto 2','Aceptado',30,41,1230,1,NULL,'2021-03-09 07:14:48'),(6,3,'Proyecto 12','Finalizado',30,1,600,1,'2021-01-31 20:35:18','2021-03-09 07:14:48'),(47,2,'Proyecto 4','Finalizado',30,10,100,1,'2021-02-01 00:45:34','2021-03-09 07:14:49'),(48,2,'Proyecto 3','Aceptado',30,19,570,1,'2021-02-01 00:47:44','2021-03-09 07:14:49'),(49,2,'Proyecto 4','Aceptado',10,10,100,1,'2021-02-01 00:48:29','2021-03-09 07:14:49'),(50,2,'Proyecto 9','Propuesto',10,1,100,1,'2021-02-03 03:26:19','2021-03-09 07:14:49'),(52,2,'Proyecto 10','Aceptado',10,15,150,1,'2021-02-03 03:33:53','2021-03-09 07:14:49'),(53,4,'Proyecto 4','Aceptado',30,53,1590,1,'2021-02-11 01:40:37','2021-03-09 07:14:48'),(54,1,'Proyecto 13','Propuesto',10,1,100,1,'2021-02-17 06:12:47','2021-03-09 07:14:49'),(55,2,'Proyecto 11','Anulado',150,27,4050,1,'2021-02-24 00:28:06','2021-03-09 07:14:49'),(57,1,'Proyecto 14','Anulado',5,25,1,1,'2021-03-01 04:24:43','2021-03-09 07:14:49'),(60,26,'MELOMANÍA WEB','Aceptado',30,37,1110,1,'2021-03-08 04:51:37','2021-03-09 07:14:48'),(61,27,'Web Empresa 11','Aceptado',30,11,330,1,'2021-03-08 06:19:30','2021-03-09 07:14:48');
/*!40000 ALTER TABLE `proyecto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requerimiento`
--

DROP TABLE IF EXISTS `requerimiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `requerimiento` (
  `idRequerimiento` int(11) NOT NULL AUTO_INCREMENT,
  `idProyecto` int(11) DEFAULT NULL,
  `nombreRequerimiento` varchar(300) DEFAULT NULL,
  `tiempoRequerimiento` int(11) DEFAULT NULL,
  `etapaRequerimiento` int(11) DEFAULT NULL,
  `descripcionRequerimiento` text,
  `comentariosRequerimiento` text,
  `estado` tinyint(4) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`idRequerimiento`),
  KEY `idProyecto_Requerimiento_FK` (`idProyecto`),
  CONSTRAINT `idProyecto_Requerimiento_FK` FOREIGN KEY (`idProyecto`) REFERENCES `proyecto` (`idProyecto`)
) ENGINE=InnoDB AUTO_INCREMENT=304 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requerimiento`
--

LOCK TABLES `requerimiento` WRITE;
/*!40000 ALTER TABLE `requerimiento` DISABLE KEYS */;
INSERT INTO `requerimiento` VALUES (16,50,'req1',1,NULL,'11111111111',NULL,1,'2021-02-11 01:30:24','2021-02-11 01:30:24'),(17,50,'req2',2,NULL,'2222222222',NULL,1,'2021-02-11 01:30:24','2021-02-11 01:30:24'),(18,50,'req3',3,NULL,'3333333333',NULL,1,'2021-02-11 01:30:24','2021-02-11 01:30:24'),(19,50,'req4',4,NULL,'4444444444',NULL,1,'2021-02-11 01:30:24','2021-02-11 01:30:24'),(230,57,'Requerimiento 1',10,NULL,'Realizar el requerimiento 1',NULL,1,'2021-03-01 04:25:25','2021-03-01 04:25:25'),(231,57,'Requerimiento 2',15,NULL,'Realizar el requerimiento 2',NULL,1,'2021-03-01 04:25:25','2021-03-01 04:25:25'),(250,1,'req1',9,NULL,'requerimiento 1',NULL,1,'2021-03-01 05:02:07','2021-03-01 05:02:07'),(251,1,'req2',3,NULL,'requerimiento 2',NULL,1,'2021-03-01 05:02:07','2021-03-01 05:02:07'),(252,1,'req3',5,NULL,'requerimiento 3',NULL,1,'2021-03-01 05:02:07','2021-03-01 05:02:07'),(253,1,'req4',15,NULL,'requerimiento 4',NULL,1,'2021-03-01 05:02:07','2021-03-01 05:02:07'),(254,1,'req5',7,NULL,'requerimiento 5',NULL,1,'2021-03-01 05:02:07','2021-03-01 05:02:07'),(255,1,'req6',13,NULL,'requerimiento 6',NULL,1,'2021-03-01 05:02:07','2021-03-01 05:02:07'),(256,2,'req3',41,NULL,'dasdsadsadsadas',NULL,1,'2021-03-01 05:02:30','2021-03-01 05:02:30'),(257,48,'Req1',19,NULL,'req1',NULL,1,'2021-03-01 05:02:50','2021-03-01 05:02:50'),(258,52,'req1',15,NULL,'req1',NULL,1,'2021-03-01 05:03:13','2021-03-01 05:03:13'),(259,53,'requerimiento 1',10,NULL,'111111111111111',NULL,1,'2021-03-01 05:03:30','2021-03-01 05:03:30'),(260,53,'requerimiento 2',15,NULL,'2222222222222',NULL,1,'2021-03-01 05:03:30','2021-03-01 05:03:30'),(261,53,'requerimiento 3',10,NULL,'3333333333333',NULL,1,'2021-03-01 05:03:30','2021-03-01 05:03:30'),(262,53,'requerimiento 4',18,NULL,'4444444444444',NULL,1,'2021-03-01 05:03:30','2021-03-01 05:03:30'),(265,55,'Login',12,NULL,'El login debe estar implementado con Wordpress',NULL,1,'2021-03-03 03:45:16','2021-03-03 03:45:16'),(266,55,'Mantenedores',15,NULL,'Implementar mantenedores para compra y venta',NULL,1,'2021-03-03 03:45:16','2021-03-03 03:45:16'),(293,61,'Login',1,NULL,'login implementado con correo electrónico',NULL,1,'2021-03-08 06:20:03','2021-03-08 06:20:03'),(294,61,'Módulo de clientes',10,NULL,'creación de clientes',NULL,1,'2021-03-08 06:20:04','2021-03-08 06:20:04'),(295,60,'Login',1,NULL,'Ingreso por e-mail, olvidar contraseña, verificación de identidad al correo electrónico',NULL,1,'2021-03-09 04:55:26','2021-03-09 04:55:26'),(296,60,'Navbar',2,NULL,'Visualización de productos por categorías y bandas',NULL,1,'2021-03-09 04:55:26','2021-03-09 04:55:26'),(297,60,'Listado de productos',5,NULL,'El sistema permite visualizar los productos con su imagen, código, descripción y precio. También permite agregar al carrito de compras',NULL,1,'2021-03-09 04:55:26','2021-03-09 04:55:26'),(298,60,'Módulo de compra',10,NULL,'Carrito de compras, código de descuento. Se permite ingresar dirección para envíos a domicilio o seleccionar opción de recojo en tienda',NULL,1,'2021-03-09 04:55:26','2021-03-09 04:55:26'),(299,60,'Autenticación por Niubiz',5,NULL,'El sistema permite el uso de todo tipo de tarjetas y métodos de pago virtuales',NULL,1,'2021-03-09 04:55:26','2021-03-09 04:55:26'),(300,60,'Módulo usuario',3,NULL,'El sistema permite la creación de un perfil de usuario',NULL,1,'2021-03-09 04:55:26','2021-03-09 04:55:26'),(301,60,'Envío de factura',2,NULL,'El sistema envía la factura en PDF al correo del usuario registrado',NULL,1,'2021-03-09 04:55:26','2021-03-09 04:55:26'),(302,60,'Seguimiento pedido',5,NULL,'El usuario tiene acceso al rastreo del pedido',NULL,1,'2021-03-09 04:55:26','2021-03-09 04:55:26'),(303,60,'Personalización del producto',4,NULL,'El usuario puede escoger el diseño de la prenda, el color, la talla',NULL,1,'2021-03-09 04:55:26','2021-03-09 04:55:26');
/*!40000 ALTER TABLE `requerimiento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `root`
--

DROP TABLE IF EXISTS `root`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `root` (
  `idRoot` int(11) NOT NULL,
  `email` varchar(300) DEFAULT NULL,
  `password` varchar(300) DEFAULT NULL,
  `estado` tinyint(4) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`idRoot`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `root`
--

LOCK TABLES `root` WRITE;
/*!40000 ALTER TABLE `root` DISABLE KEYS */;
INSERT INTO `root` VALUES (1,'douglaszs397@gmail.com','e10adc3949ba59abbe56e057f20f883e',1,NULL,NULL);
/*!40000 ALTER TABLE `root` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `session_id` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `expires` int(10) unsigned NOT NULL,
  `data` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
INSERT INTO `sessions` VALUES ('GxNJfbPfwFDjo85lIPoRUuqf3ejEPMj-',1615330898,'{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"},\"flash\":{},\"root\":{\"idRoot\":1,\"email\":\"douglaszs397@gmail.com\",\"password\":\"e10adc3949ba59abbe56e057f20f883e\",\"estado\":1,\"createdAt\":null,\"updatedAt\":null}}'),('SBq4--xIzroWZTq_5FSkUXaW4dT5ss09',1615361078,'{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"},\"flash\":{},\"root\":{\"idRoot\":1,\"email\":\"douglaszs397@gmail.com\",\"password\":\"e10adc3949ba59abbe56e057f20f883e\",\"estado\":1,\"createdAt\":null,\"updatedAt\":null}}');
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venta`
--

DROP TABLE IF EXISTS `venta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venta` (
  `idVenta` int(11) NOT NULL AUTO_INCREMENT,
  `numeroSerieVenta` varchar(50) DEFAULT NULL,
  `idEmpresaVenta` int(11) DEFAULT NULL,
  `idProyecto` int(11) DEFAULT NULL,
  `tituloVenta` varchar(300) DEFAULT NULL,
  `numeroFactura` varchar(45) DEFAULT NULL,
  `descripcionVenta` text,
  `adjunto` varchar(200) DEFAULT NULL,
  `neto` decimal(10,2) DEFAULT NULL,
  `igv` decimal(10,2) DEFAULT NULL,
  `totalVenta` decimal(10,2) DEFAULT NULL,
  `fechaVenta` date DEFAULT NULL,
  `fechaDeclaracion` date DEFAULT NULL,
  `estado` tinyint(4) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`idVenta`),
  KEY `idEmpresa_Venta_FK` (`idEmpresaVenta`),
  KEY `idProyecto_Venta_FK` (`idProyecto`),
  CONSTRAINT `idEmpresa_Venta_FK` FOREIGN KEY (`idEmpresaVenta`) REFERENCES `empresa` (`idEmpresa`),
  CONSTRAINT `idProyecto_Venta_FK` FOREIGN KEY (`idProyecto`) REFERENCES `proyecto` (`idProyecto`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venta`
--

LOCK TABLES `venta` WRITE;
/*!40000 ALTER TABLE `venta` DISABLE KEYS */;
INSERT INTO `venta` VALUES (1,'1022658741811111',1,1,'Venta 1','11111','Venta número 1',NULL,9111.02,1999.98,11111.00,'2020-01-01','2020-01-01',1,'2021-03-08 00:26:37','2021-03-08 00:26:37'),(2,'1254879585222222',2,2,'Venta 2','22222','Venta número 2',NULL,18222.04,3999.96,22222.00,'2020-02-02','2020-02-02',1,'2021-03-08 00:28:22','2021-03-08 00:28:22'),(3,'4875885958833333',3,48,'Venta 3','33333','Venta número 3',NULL,27333.06,5999.94,33333.00,'2020-03-03','2020-03-03',1,'2021-03-08 00:30:59','2021-03-08 00:30:59'),(4,'2154878588544444',4,53,'Venta 4','44444','Venta número 4',NULL,36444.08,7999.92,44444.00,'2020-04-04','2020-04-04',1,'2021-03-08 00:34:28','2021-03-08 00:34:28'),(29,'10739049259000',26,60,'Desarrollo Melomanía Web','000','Desarrollo del e-commerce Melomanía',NULL,910.20,199.80,1110.00,'2021-03-08','2021-03-08',1,'2021-03-08 05:18:25','2021-03-08 06:08:22'),(30,'111111111111111',27,61,'Desarrollo proyecto 11','1111','Desarrollo del proyecto número 11',NULL,270.60,59.40,330.00,'2021-03-08','2021-03-08',1,'2021-03-08 06:21:56','2021-03-09 01:36:24'),(31,'1111111111111111',27,61,'Desarrollo 11','11111','Desarrollo 11.2',NULL,270.60,59.40,330.00,'2021-03-08','2021-03-08',1,'2021-03-08 06:42:36','2021-03-08 06:42:36');
/*!40000 ALTER TABLE `venta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ventaRecurrente`
--

DROP TABLE IF EXISTS `ventaRecurrente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ventaRecurrente` (
  `idVentaRecurrente` int(11) NOT NULL AUTO_INCREMENT,
  `numeroSerieVentaRecurrente` varchar(50) DEFAULT NULL,
  `idEmpresaVentaRecurrente` int(11) DEFAULT NULL,
  `idProyecto` int(11) DEFAULT NULL,
  `tituloVentaRecurrente` varchar(300) DEFAULT NULL,
  `numeroFacturaVentaRecurrente` varchar(45) DEFAULT NULL,
  `descripcionVentaRecurrente` text,
  `adjunto` varchar(200) DEFAULT NULL,
  `neto` decimal(10,2) DEFAULT NULL,
  `igv` decimal(10,2) DEFAULT NULL,
  `totalVentaRecurrente` decimal(10,2) DEFAULT NULL,
  `periodoRecurrencia` int(11) DEFAULT NULL,
  `fechaVentaRecurrente` date DEFAULT NULL,
  `fechaDeclaracion` date DEFAULT NULL,
  `estado` tinyint(4) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`idVentaRecurrente`),
  KEY `idEmpresa_VentaRecurrente_FK` (`idEmpresaVentaRecurrente`),
  KEY `idProyecto_VentaRecurrente_FK` (`idProyecto`),
  CONSTRAINT `idEmpresa_VentaRecurrente_FK` FOREIGN KEY (`idEmpresaVentaRecurrente`) REFERENCES `empresa` (`idEmpresa`),
  CONSTRAINT `idProyecto_VentaRecurrente_FK` FOREIGN KEY (`idProyecto`) REFERENCES `proyecto` (`idProyecto`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ventaRecurrente`
--

LOCK TABLES `ventaRecurrente` WRITE;
/*!40000 ALTER TABLE `ventaRecurrente` DISABLE KEYS */;
INSERT INTO `ventaRecurrente` VALUES (1,'102265874181111',1,1,'Venta Recurrente 1','1111','Venta recurrente número 1',NULL,911.02,199.98,1111.00,3,'2021-03-01','2020-03-01',1,'2021-03-04 04:11:24','2021-03-09 05:00:00'),(2,'125487958522222',2,2,'Venta Recurrente 2','2222','Venta recurrente número 2',NULL,1822.04,399.96,2222.00,3,'2021-03-02','2020-03-02',1,'2021-03-04 04:32:45','2021-03-09 05:00:00'),(3,'',3,48,'Venta Recurrente 3',NULL,'Venta recurrente número 3',NULL,273.06,59.94,333.00,3,'2021-03-03','2020-03-03',1,'2021-03-07 23:10:24','2021-03-09 05:00:00'),(5,NULL,4,53,'Venta Recurrente 4',NULL,'Esta es la venta recurrente número 4',NULL,364.08,79.92,444.00,3,'2021-03-04','2020-03-04',1,'2021-03-08 00:02:58','2021-03-09 05:00:00'),(6,NULL,12,50,'Venta Recurrente 5',NULL,'Venta recurrente número 5',NULL,45555.10,9999.90,55555.00,5,'2021-08-05','2020-05-05',1,'2021-03-08 01:59:34','2021-03-08 02:24:00'),(7,NULL,13,6,'Venta Recurrente 6',NULL,'Venta recurrente número 6',NULL,5466.12,1199.88,6666.00,6,'2021-06-06','2020-06-06',1,'2021-03-08 02:02:03','2021-03-08 02:20:00'),(8,NULL,14,54,'Venta Recurrente 7',NULL,'Venta recurrente número 7',NULL,6377.14,1399.86,7777.00,3,'2021-04-21','2021-04-21',1,'2021-03-08 02:12:50','2021-03-08 03:55:27'),(9,NULL,15,54,'Venta Recurrente 8',NULL,'Venta recurrente número 8',NULL,72888.16,15999.84,88888.00,8,'2021-04-08','2020-08-08',1,'2021-03-08 02:19:20','2021-03-08 02:20:00'),(10,NULL,16,50,'Venta Recurrente 9',NULL,'Venta recurrente número 9',NULL,81999.18,17999.82,99999.00,9,'2021-06-09','2020-09-09',1,'2021-03-08 02:21:14','2021-03-08 02:24:00'),(11,NULL,17,52,'Venta Recurrente 10',NULL,'Venta recurrente número 10',NULL,8282.82,1818.18,10101.00,10,'2021-08-10','2020-10-10',1,'2021-03-08 02:29:22','2021-03-08 02:31:00');
/*!40000 ALTER TABLE `ventaRecurrente` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-09  7:26:31
