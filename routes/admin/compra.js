var express = require('express');
//const Models = require('../../models');
const { Sequelize, DataTypes, Op } = require('sequelize'); //Cargamos la Libreria
const { Console } = require('console');
var router = express.Router();
//var multer = require('multer');
//svar upload = multer({ dest: 'public/uploads/files' });
var moment = require('moment');
moment.locale('es');

// ==================== COMPRAS ====================

router.get("", async function (req, res, next) {

  res.locals.tituloForm = "Lista de Compras";
  res.locals.title = "Lista de Compras";
  res.locals.content = 'compra/list';

  const limit = 6;
  let p = req.query.p;

  if (p == undefined) {
    p = 0;
  }

  let filter = { where: { estado: 1, fechaDeclaracion: { [Op.not]: null } } };

  res.locals.search = '';

  if (req.query.search) {
    res.locals.search = req.query.search;
    // BÚSQUEDA POR AÑO Y MES DE DECLARACIÓN DE COMPRAS
    filter.where.fechaDeclaracion = { [Op.substring]: req.query.search };
  }

  const count = await Models.Compra.count(filter);
  const pages = parseInt(count / limit) + parseInt((count % limit == 0) ? 0 : 1);

  res.locals.pages = Array.from(Array(pages).keys());
  res.locals.currentpage = p;

  filter.limit = limit;
  filter.offset = p * limit;
  let compras = await Models.Compra.findAll(filter);

  res.locals.compras = compras;

  let empresasId = [];
  for (compra of compras) {
    empresasId.push(compra.idEmpresaCompra);
  }

  filter = { where: { estado: 1, idEmpresa: empresasId } };
  let empresas = await Models.Empresa.findAll(filter);

  let objEmpresa = {};

  for (empresa of empresas) {
    objEmpresa[empresa.idEmpresa] = empresa;
  }

  res.locals.empresa = objEmpresa;

  console.log("ENTRÓ EN LIST COMPRAS");

  res.render("page");
});


router.get("/create", async function (req, res, next) {

  res.locals.content = 'compra/form';
  res.locals.tituloForm = "Registrar Compras";
  res.locals.title = "Registrar Compras";

  let filter = { where: { estado: 1 } };
  res.locals.empresas = await Models.Empresa.findAll(filter);

  filter = { where: { estado: 1 } };
  res.locals.proyectos = await Models.Proyecto.findAll(filter);

  res.locals.obj = Models.Compra.build();

  let fechaActual = moment();   // Obtener fecha actual

  fechaActual = moment(fechaActual).format('YYYY-MM-DD');

  res.locals.obj = { fechaCompra: fechaActual, fechaDeclaracion: null };

  console.log("FECHA ACTUAL: ", fechaActual);

  res.render('page');
});


let processsFile = function (req, res, next) {

  if (req.files.length == 0) {
    // NO HAY ARCHIVOS
    return next();
  }

  let anio = null;

  if (req.body.idCompra != undefined) {
    anio = moment(req.body.fechaCompra).format('YYYY');
  }


  if (req.body.idVenta != undefined) {
    anio = req.body.fechaVenta;
  }

  let path = `./upload/files/${anio}`;
  const fs = require('fs');
  if (!fs.existsSync(path)) {
    fs.mkdirSync(path);
    console.log("MKDIR ", path);
  }

  path = `${path}/${req.empresa.rucEmpresa}`;
  if (!fs.existsSync(path)) {
    fs.mkdirSync(path);
    console.log("MKDIR ", path);
  }


  for (f of req.files) {
    fs.rmSync(f.filename, path);

  }


  return next();
}



let processEmpresa = async function (req, res, next) {
  let filter = {};
  //filter idempresa
  let empresa = await Models.Empresa.findOne(filter);

  req.empresa = empresa;

  return next();
};

router.post("/save", upload.any(), processEmpresa, processsFile, async function (req, res, next) {

  console.log("REQ.BODY: ", req.body);
  console.log("REQ.FILES: ", req.files);

  let filter = { where: { idCompra: req.body.idCompra } };
  console.log("FILTER: ", filter);
  const [c, created] = await Models.Compra.findOrCreate(filter);

  // si se está creando una compra...
  if (c.numeroSerieCompra == undefined) {

    filter = { where: { estado: 1, idEmpresaCompra: req.empresa.idEmpresa, numeroSerieCompra: req.body.numeroSerieCompra } };
    let n = await Models.Compra.count(filter);

    if (n > 0) {
      console.log("DATOS INVÁLIDOS");
      req.flash('error', 'El número de factura ya existe.');
      res.redirect("/admin/compra/");
      return;
    }

    c.estado = 1;

  }

  c.numeroSerieCompra = req.body.numeroSerieCompra;
  c.idEmpresaCompra = req.empresa.idEmpresa;

  if (req.body.idProyecto == "0") {
    c.idProyecto = null;
  } else {
    c.idProyecto = req.body.idProyecto; // REGISTRAR UN PROYECTO EN COMPRA ES OPCIONAL
  }

  c.tituloCompra = req.body.tituloCompra;
  c.numeroFactura = req.body.numeroFactura;
  c.descripcionCompra = req.body.descripcionCompra;

  c.totalCompra = req.body.totalCompra;
  c.igv = req.body.igv;
  c.neto = req.body.neto;

  c.fechaCompra = req.body.fechaCompra;
  /* 
    if (req.body.fechaDeclaracion) {
      c.fechaDeclaracion = req.body.fechaDeclaracion;
    }
   */

  if (req.files.length > 0) {
    let file = req.files[0];

    if (file.mimetype != 'application/pdf' && file.mimetype != 'image/jpeg') {
      console.log("FILE NOT FORMAT", file.mimetype, file.originalname);
      req.flash('info', 'Formato de archivo incorrecto');
      res.redirect("/admin/compra/");
      return;
    }

    c.adjunto = file.filename;

  }

  await c.save();

  res.redirect('/admin/compra/');
});


router.get("/edit/:idCompra", async function (req, res, next) {

  res.locals.content = 'compra/form';
  res.locals.tituloForm = "Editar Compra";
  res.locals.title = "Editar Compra";


  let filter = { where: { estado: 1 } };
  res.locals.empresas = await Models.Empresa.findAll(filter);

  filter = { where: { estado: 1 } };
  res.locals.proyectos = await Models.Proyecto.findAll(filter);

  res.locals.obj = await Models.Compra.findByPk(req.params.idCompra);

  console.log("FECHA DE DECLARACIÓN: ", res.locals.obj.fechaDeclaracion);

  res.locals.empresaSeleccionada = "selected";

  res.render('page');
});


router.get("/delete/:idCompra", async function (req, res, next) {

  res.locals.compra = { idCompra: req.params.idCompra };

  res.locals.content = 'compra/delete';
  res.locals.tituloForm = "Eliminar Compra";
  res.locals.title = "Eliminar Compra";

  res.locals.obj = await Models.Compra.findByPk(req.params.idCompra);

  res.render('page');
});


router.post("/delete/:idCompra", async function (req, res, next) {

  let obj = await Models.Compra.findByPk(req.params.idCompra);
  obj.estado = 0;

  await obj.save();

  // Si es una compra pendiente...
  if (obj.fechaDeclaracion == undefined) {
    res.redirect('/admin/compra/pendientes');

    return;
  }

  res.redirect('/admin/compra/');
});


router.get("/pendientes", async function (req, res, next) {
  filter = { where: { estado: 1, fechaDeclaracion: null } };
  let compras = await Models.Compra.findAll(filter);

  res.locals.compras = compras;

  let fechaActual = moment();   // Obtener fecha actual

  res.locals.mesDeclaracion = moment(fechaActual).format('YYYY-MM');
  console.log("MES DECLARACIÓN: ", res.locals.mesDeclaracion);

  let empresasId = [];
  for (compra of compras) {
    empresasId.push(compra.idEmpresaCompra);
  }

  filter = { where: { estado: 1, idEmpresa: empresasId } };
  let empresas = await Models.Empresa.findAll(filter);

  let objEmpresa = {};

  for (empresa of empresas) {
    objEmpresa[empresa.idEmpresa] = empresa;
  }

  res.locals.empresa = objEmpresa;

  res.locals.pages = [0];
  res.locals.currentpage = 0;

  res.locals.content = 'compra/pendiente';
  res.locals.tituloForm = "Compras por declarar";
  res.locals.title = "Compras por declarar";

  console.log("ENTRÓ EN COMPRAS PENDIENTES");

  res.render('page');
});

router.post("/pendientes/save/:mesDeclaracion", async function (req, res, next) {
  console.log("");
  console.log("COMPRAS PENDIENTE EN RUTA POST: ", JSON.parse(req.body.idComprasPendientes));
  console.log("MES DECLARACIÓN: ", req.params.mesDeclaracion);

  if (req.body.idComprasPendientes == undefined) {
    res.redirect('admin/compra/pendientes/');

    return;
  }

  let arrIDs = [];
  arrIDs = JSON.parse(req.body.idComprasPendientes);

  let filter = { where: { estado: 1, idCompra: arrIDs } };
  let comprasPendientes = await Models.Compra.findAll(filter);

  for (compra of comprasPendientes) {
    compra.fechaDeclaracion = req.params.mesDeclaracion;

    await compra.save();
  }

  console.log("GUARDANDO Y REDIRIGIENDO A LIST COMPRAS...");

  res.redirect('/admin/compra/');
});

module.exports = router;