var express = require('express');
const { Sequelize, DataTypes, Op } = require('sequelize');
const Models = require('../../models');
var router = express.Router();

// ==================== EMPRESA ====================

let listEmpresas = async function (req, res, next) {

  res.locals.tituloForm = "Lista de Empresas";
  res.locals.title = "Lista de Empresas";
  res.locals.content = 'empresa/list';

  const limit = 6;
  let p = req.query.p;

  if (p == undefined) {
    p = 0;
  }

  let razonesSociales = [];
  let filter = { where: { estado: 1 } };

  res.locals.search = '';

  if (req.query.search) {

    res.locals.search = req.query.search;

    // búsqueda por RUC
    if (req.query.search % 1 == 0) {
      console.log("Entró en RUC: ", req.query.search);

      filter.where.rucEmpresa = { [Op.like]: `%${req.query.search}%` };

    } else if (typeof req.query.search === 'string') { // búsqueda por nombre

      filter.where[Sequelize.Op.or] = [
        { razonSocialEmpresa: { [Op.like]: `%${req.query.search}%` } },
        { emailEmpresa: { [Op.like]: `%${req.query.search}%` } }
      ];
    }

  }

  filter.order = [['importancia', 'DESC']];

  const count = await Models.Empresa.count(filter);
  const pages = parseInt(count / limit) + parseInt((count % limit == 0) ? 0 : 1);

  res.locals.pages = Array.from(Array(pages).keys());
  res.locals.currentpage = p;

  filter.limit = limit;
  filter.offset = p * limit;

  res.locals.empresas = await Models.Empresa.findAll(filter);

  res.render("page");
};


router.post("/", async function (req, res, next) {


}, listEmpresas);

router.get("/", listEmpresas);


router.get("/create", async function (req, res, next) {

  res.locals.content = 'empresa/form';
  res.locals.tituloForm = "Registrar Empresa";
  res.locals.title = "Registrar Empresa";

  res.locals.obj = Models.Empresa.build();
  res.locals.checked = "";

  console.log("OBJ: ", res.locals.obj);

  res.render('page');
});


router.post("/save", async function (req, res, next) {

  let filter = { where: { idEmpresa: req.body.idEmpresa } };
  console.log("Filter: ", filter);
  const [e, created] = await Models.Empresa.findOrCreate(filter);

  // si se está creando una empresa...
  // también si created == true
  if (e.rucEmpresa == undefined) {

    filter = { where: { estado: 1, rucEmpresa: req.body.rucEmpresa } };
    let n = await Models.Empresa.count(filter);

    if (n > 0) {
      console.log("RUC INVÁLIDO");
      req.flash('error', 'El número de RUC ingresado ya existe.');
      res.redirect("/admin/empresa/");
      return;
    }

    e.estado = 1;

  }

  console.log("REQ.BODY: ", req.body);

  e.razonSocialEmpresa = req.body.razonSocialEmpresa;
  e.rucEmpresa = req.body.rucEmpresa;
  e.emailEmpresa = req.body.emailEmpresa;
  e.importancia = (req.body.importancia == 1) ? 1 : 0;

  await e.save();

  res.redirect('/admin/empresa/');
});


router.get("/edit/:idEmpresa", async function (req, res, next) {

  res.locals.empresa = { idEmpresa: req.params.idEmpresa };
  res.locals.content = 'empresa/form';
  res.locals.tituloForm = 'Editar Empresa';
  res.locals.title = 'Editar Empresa';

  res.locals.obj = await Models.Empresa.findByPk(req.params.idEmpresa);

  if (res.locals.obj.importancia == 1) {
    res.locals.checked = "checked";
  } else {
    res.locals.checked = "";
  }

  res.render('page');
});


router.get("/delete/:idEmpresa", async function (req, res, next) {

  res.locals.content = 'empresa/delete';
  res.locals.tituloForm = 'Eliminar Empresa';
  res.locals.title = 'Eliminar Empresa';

  res.locals.obj = await Models.Empresa.findByPk(req.params.idEmpresa);

  res.render('page');
});


router.post("/delete/:idEmpresa", async function (req, res, next) {

  let obj = await Models.Empresa.findByPk(req.params.idEmpresa);/*
  
  obj.estado = 0;
  await obj.save();*/

  await obj.delete();

  res.redirect("/admin/empresa/");
});


router.get("/:rucEmpresa/detalle/:tabSeleccionado", async function (req, res, next) {

  res.locals.content = 'empresa/detalle';
  res.locals.tituloForm = 'Detalle Empresa';
  res.locals.title = 'Detalle Empresa';

  let filter = { where: { estado: 1, rucEmpresa: req.params.rucEmpresa } };
  var empresa = await Models.Empresa.findOne(filter);

  if (req.params.tabSeleccionado == "proyectos") {
    
    filter = { where: { estado: 1, idEmpresa: empresa.idEmpresa, estadoProyecto: "Aceptado" } };
    let proyectos = await Models.Proyecto.findAll(filter);
    res.locals.proyectos = proyectos;

  } 
  if (req.params.tabSeleccionado == "compras") {
    
    filter = { where: { estado: 1, idEmpresaCompra: empresa.idEmpresa } };
    let compras = await Models.Compra.findAll(filter);
    res.locals.compras = compras;

  }
  if (req.params.tabSeleccionado == "ventas") {
    
    filter = { where: { estado: 1, idEmpresaVenta: empresa.idEmpresa } };
    let ventas = await Models.Venta.findAll(filter);
    res.locals.ventas = ventas;
    
  }
    
  res.locals.empresa = empresa;
  res.locals.tabSeleccionado = req.params.tabSeleccionado;

  res.render('page');
});

module.exports = router;