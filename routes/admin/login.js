var express = require('express');
const Models = require('../../models');
var router = express.Router();

router.get('/logout', function (req, res, next) {

  req.session.root = undefined;

  res.redirect("/admin/login");
  return;
});


router.get('/login', function (req, res, next) {
  
  res.locals.title = "Login";

  console.log("ENTRÓ A LOGIN");
  res.render('login/login');
});


router.post('/login', async function (req, res, next) {

  const md5 = require('md5');
  
  let password = md5(req.body.password);

  let filter = { where: { estado: 1, email: req.body.email, password: password } };
  let roots = await Models.Root.findAll(filter);

  if (roots.length <= 0) {    
    req.flash('error', 'E-mail o contraseña incorrectos.');
    res.redirect("/admin/login");

    return;
  }

  req.session.root = roots[0];

  res.redirect("/admin/proyecto/aceptado");

  return;
});


module.exports = router;