var express = require('express');
const Models = require('../../models');
var router = express.Router();
var flag = false;

// ==================== PROYECTO ====================

let listProyectos = async function (req, res, next) {

  res.locals.tituloForm = "Lista de Proyectos";
  res.locals.title = "Lista de Proyectos";
  res.locals.content = "proyecto/list";

  res.locals.state = req.params.state;
  console.log("ESTADO PARÁMETRO: ", req.params.state);
  console.log("ESTADO: ", res.locals.state);
  

  let filter = { where: { estado: 1, estadoProyecto: req.params.state } };
  let proyectos = await Models.Proyecto.findAll(filter);
  res.locals.proyectos = proyectos;

  let empresasProyecto = [];
  for (proyecto of proyectos) {
    empresasProyecto.push(proyecto.idEmpresa);
  }

  filter = { where: { estado: 1, idEmpresa: empresasProyecto } };
  let empresas = await Models.Empresa.findAll(filter);

  let objEmpresa = {};

  for (empresa of empresas) {
    objEmpresa[empresa.idEmpresa] = empresa;
  }

  flag = false;

  res.locals.empresa = objEmpresa;

  res.render("page");
};


router.get("/create", async function (req, res, next) {

  console.log("SE ESTÁ CREANDO EL PROYECTO...");

  res.locals.tituloForm = 'Registrar Proyecto';
  res.locals.title = 'Registrar Proyecto';
  res.locals.content = 'proyecto/form';
  res.locals.accion = '';

  let filter = { where: { estado: 1 } };
  res.locals.empresas = await Models.Empresa.findAll(filter);


  res.locals.obj = Models.Proyecto.build();
  res.locals.objReq = Models.Requerimiento.build();

  res.render('page');
});


router.post("/save", async function (req, res, next) {

  console.log("DATOS A GUARDAR: ", req.body);
  let filter = { where: { idProyecto: req.body.idProyecto } };
  const [p, created] = await Models.Proyecto.findOrCreate(filter);

  filter = { where: { idEmpresa: req.body.idEmpresa } };
  let empresa = await Models.Empresa.findOne(filter);

  p.nombreProyecto = req.body.nombreProyecto;
  //p.idEmpresa = req.body.idEmpresa;
  p.idEmpresa = empresa.idEmpresa;
  p.estadoProyecto = req.body.estadoProyecto;
  p.costoDia = req.body.costoDia;

  if (req.body.costoTotal != '') {
    p.costoTotal = req.body.costoTotal;
  }

  // consulta de todos los requerimientos del proyecto a guardar
  filter = { where: { estado: 1, idProyecto: req.body.idProyecto } };
  let requerimientosProy = await Models.Requerimiento.findAll(filter);

  let duracionProy = 0;
  for (req of requerimientosProy) {
    // si el requerimiento tiene duración...
    if (req.tiempoRequerimiento != undefined) {
      duracionProy += req.tiempoRequerimiento;
    }
  }

  p.totalDias = duracionProy;

  if (created) {
    p.estado = 1;
  }

  await p.save();

  console.log("FLAG: ", flag);

  console.log("DATOS GUARDADOS: ", p);

  // entrar si se creó por primera vez para pasar a editar los requerimientos
  if (p.estado == 1 && !flag) {

    console.log("FLAG EDIT?: ", flag);
    flag = true;

    filter = { where: { idProyecto: p.idProyecto, estado: 1 } };
    res.locals.requerimientos = await Models.Requerimiento.findAll(filter);

    console.log("REQS: ", res.locals.requerimientos);

    console.log("SE CREÓ EL PROYECTO Y SE REDIRECCIONA A EDIT");
    res.redirect(`/admin/proyecto/edit/${p.idProyecto}/`);

    return;
  }

  console.log("FLAG SE VA A LIST SI ES TRUE: ", flag);

  res.redirect('/admin/proyecto/aceptado');
});

router.post("/:idProyecto/requerimientos/save", async function (req, res, next) {

  console.log("");
  console.log("REQUERIMIENTOS");

  console.log("REQ.BODY: ", req.body);

  console.log("");
  var reqs = JSON.parse(req.body.reqs);
  console.log("REQS: ", reqs);

  let filter = { where: { estado: 1, idProyecto: req.params.idProyecto } };
  let reqsPasados = await Models.Requerimiento.findAll(filter);

  for (reqPasado of reqsPasados) {
    reqPasado.estado = 0;
    await reqPasado.save();
  }

  for (requerimiento of reqs) {

    let objReqPro = Models.Requerimiento.build();
    objReqPro.idProyecto = req.params.idProyecto;
    objReqPro.nombreRequerimiento = requerimiento.nombreRequerimiento;
    objReqPro.tiempoRequerimiento = requerimiento.tiempoRequerimiento;
    objReqPro.descripcionRequerimiento = requerimiento.descripcionRequerimiento;
    objReqPro.estado = 1;
    //objReqPro.etapaRequerimiento = requerimiento.etapaRequerimiento;
    //objReqPro.comentariosRequerimiento = requerimiento.comentariosRequerimiento;

    await objReqPro.save();

    console.log("DATOS RECIBIDOS Y GUARDADOS: ", objReqPro);
  }

  console.log("SE GUARDARON LOS REQUERIMIENTOS");

  res.redirect(`/admin/proyecto/edit/${req.params.idProyecto}/`);
});


router.get("/edit/:idProyecto", async function (req, res, next) {

  res.locals.proyecto = { idProyecto: req.params.idProyecto };
  res.locals.content = 'proyecto/form';
  res.locals.tituloForm = 'Editar Proyecto';
  res.locals.title = 'Editar Proyecto';
  res.locals.accion = 'edit';

  res.locals.obj = await Models.Proyecto.findByPk(req.params.idProyecto);

  let filter = { where: { estado: 1 } };
  res.locals.empresas = await Models.Empresa.findAll(filter);

  filter = { where: { idProyecto: req.params.idProyecto, estado: 1 } };
  res.locals.requerimientos = await Models.Requerimiento.findAll(filter);


  res.render('page');
});

router.get("/delete/:idProyecto", async function (req, res, next) {

  res.locals.content = 'proyecto/delete';
  res.locals.tituloForm = 'Eliminar Proyecto';
  res.locals.title = 'Eliminar Proyecto';

  res.locals.obj = await Models.Proyecto.findByPk(req.params.idProyecto);

  res.render('page');
});

router.post("/delete/:idProyecto", async function (req, res, next) {

  let obj = await Models.Proyecto.findByPk(req.params.idProyecto);
  obj.estado = 0;
  await obj.save();

  res.redirect("/admin/proyecto/aceptado");
});

router.get("/detalle/:idProyecto", async function (req, res, next) {

  res.locals.content = 'proyecto/detalle';
  res.locals.tituloForm = 'Detalle Proyecto';
  res.locals.title = 'Detalle Proyecto';

  let filter = { where: { estado: 1, idProyecto: req.params.idProyecto } };
  let proyecto = await Models.Proyecto.findOne(filter);

  filter = { where: { estado: 1, idEmpresa: proyecto.idEmpresa } };
  let empresa = await Models.Empresa.findOne(filter);

  filter = { where: { estado: 1, idProyecto: req.params.idProyecto } };
  let requerimientos = await Models.Requerimiento.findAll(filter);

  res.locals.requerimientos = requerimientos;
  res.locals.empresa = empresa;
  res.locals.proyecto = proyecto;

  res.render('page');
});

router.get("/detalle-requerimientos/:idProyecto", async function (req, res, next) {

  res.locals.content = 'proyecto/detalleRequerimientos';
  res.locals.tituloForm = 'Detalle Requerimientos';
  res.locals.title = 'Detalle Requerimientos';

  let filter = { where: { estado: 1, idProyecto: req.params.idProyecto } };
  let proyecto = await Models.Proyecto.findOne(filter);

  filter = { where: { estado: 1, idProyecto: req.params.idProyecto } };
  let requerimientos = await Models.Requerimiento.findAll(filter);

  res.locals.requerimientos = requerimientos;
  res.locals.proyecto = proyecto;

  res.render('page');
});

/* 
router.get("/aceptado", function( req, res ,next) { req.state = "aceptado"; next(); }, listProyectos );
router.get("/finalizado", function( req, res ,next) { req.state = "finalizado"; next(); }, listProyectos );
 */

router.get("/:state", function (req, res, next) {
  
  next();
}, listProyectos);

module.exports = router;