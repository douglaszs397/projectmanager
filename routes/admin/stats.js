const express = require('express');
const router = express.Router();

const { Sequelize, DataTypes, Op } = require('sequelize'); //Cargamos la Libreria

const moment = require('moment');


const declaracionAnual = async  function(req, res, next){

    console.log(req.FecInicio, req.FecFin );

    let filter = { where: { fechaVenta: { [Sequelize.Op.between]:[req.FecInicio, req.FecFin] } } };
    let ventas = await Models.Venta.findAll(filter);

    let ventaTotal = 0;
    for ( venta of ventas ){
        ventaTotal += Math.round(parseFloat(venta.neto));
    }

    res.locals.ventas = ventas;
    res.locals.ventaTotal = ventaTotal;
    


    filter = { where: { fechaDeclaracion: { [Sequelize.Op.between]:[req.FecInicio, req.FecFin] } } };
    let compras = await Models.Compra.findAll(filter);

    let compraTotal = 0;
    for ( compra of compras ){
        if ( compra.neto == null ) continue;

        compraTotal += Math.round(parseFloat(compra.neto));
        
    }

    res.locals.compras = compras;
    res.locals.compraTotal = compraTotal;

    res.locals.netoTotal = ventaTotal - compraTotal;


    res.locals.content = 'stats';
    res.render('page');

};

const annoActual = function(req, res, next){

    let anno = null;

    anno = moment().format('YYYY');

    if ( req.params.anio ){
        
        anno = moment(`${req.params.anio}-01-01`).format('YYYY');
    }


    let Fecha = moment(`${anno}-01-01`);
    let FecInicio = Fecha.format('YYYY-MM-DD');
    let FecFin = Fecha.add(1,'year').format('YYYY-MM-DD');

    req.FecInicio = FecInicio;
    req.FecFin = FecFin;
    req.anno = anno;

    next();
};

const mesActual = function(req, res, next){
    let mes = null;
    mes = moment().format('MM');
    if ( req.params.mes ){
        mes = moment(`${req.anno}-${req.params.mes}-01`).format('MM');
    }

    let Fecha = moment(`${req.anno}-${req.mes}-01`);
    let FecInicio = Fecha.format('YYYY-MM-DD');
    let FecFin = Fecha.add(1,'month').format('YYYY-MM-DD');

    req.FecInicio = FecInicio;
    req.FecFin = FecFin;

    next();
};

router.get("/", annoActual, declaracionAnual);

router.get("/:anio", annoActual, declaracionAnual);

router.get("/:anio/:mes", annoActual, mesActual, declaracionAnual);



module.exports = router;