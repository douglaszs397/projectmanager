var express = require('express');
const Models = require('../../models');
const { Sequelize, DataTypes, Op } = require('sequelize');
var router = express.Router();
var multer = require('multer');
var upload = multer({ dest: 'public/uploads/files' })
var moment = require('moment');
var { moneda, mailgun } = require('../../config/config');

// ==================== VENTAS ====================

let processVenta = async function (req, res, next) {

  res.locals.tituloForm = "Lista de Ventas";
  res.locals.title = "Lista de Ventas";
  res.locals.content = 'venta/list';

  const limit = 6;
  let p = req.query.p;

  if (p == undefined) {
    p = 0;
  }

  let filter = { where: { estado: 1, estadoVenta: req.session.estadoVenta } };

  res.locals.search = '';

  if (req.query.search) {

    res.locals.search = req.query.search;

    filter.where.fechaDeclaracion = { [Op.substring]: req.query.search };

  }

  const count = await Models.Venta.count(filter);
  const pages = parseInt(count / limit) + parseInt((count % limit == 0) ? 0 : 1);

  res.locals.pages = Array.from(Array(pages).keys());

  res.locals.currentpage = p;

  filter.limit = limit;
  filter.offset = p * limit;

  console.log("FILTRO: ", filter);

  let ventas = await Models.Venta.findAll(filter);

  filter = { where: { estado: 1, estadoVenta: req.session.estadoVenta }, attributes: ["totalVenta", "neto", "igv"] };
  let totalesVentas = await Models.Venta.findAll(filter)

  let sumaVentas = 0;
  let sumaNeto = 0;
  let sumaIgv = 0;
  for (total of totalesVentas) {
    sumaVentas += parseFloat(total.totalVenta);
    sumaNeto += parseFloat(total.neto);
    sumaIgv += parseFloat(total.igv);
  }

  console.log("VALOR DEL SOL: ", moneda.pen);

  res.locals.ventas = ventas;
  res.locals.sumaVentas = Number(sumaVentas * moneda.pen).toFixed(2);
  res.locals.sumaNeto = Number(sumaNeto).toFixed(2);
  res.locals.sumaIgv = Number(sumaIgv).toFixed(2);


  let empresasId = [];
  for (venta of ventas) {
    empresasId.push(venta.idEmpresaVenta);
  }

  filter = { where: { idEmpresa: empresasId } };
  let empresas = await Models.Empresa.findAll(filter);

  let objEmpresa = {};

  for (empresa of empresas) {
    objEmpresa[empresa.idEmpresa] = empresa;
  }

  res.locals.empresa = objEmpresa;


  res.render("page");
};

let calcularTotalVentasFacturadas = async function (req, res, next) {

  let filter = { where: { estado: 1, estadoVenta: 'facturado' } };
  res.locals.contadorFacturadas = await Models.Venta.count(filter);
  console.log(`res.locals.contadorFacturadas`, res.locals.contadorFacturadas);

  next();
};

let calcularTotalVentasPendientes = async function (req, res, next) {

  let filter = { where: { estado: 1, estadoVenta: 'pendiente' } };
  res.locals.contadorPendientes = await Models.Venta.count(filter);
  console.log(`res.locals.contadorPendientes`, res.locals.contadorPendientes);

  next();
};

router.get("/facturado", function (req, res, next) {

  res.locals.tabSeleccionado = 'facturado';
  req.session.estadoVenta = 'facturado';

  next();
}, calcularTotalVentasFacturadas, calcularTotalVentasPendientes, processVenta);

router.get("/pendiente", function (req, res, next) {

  res.locals.tabSeleccionado = 'pendiente';
  req.session.estadoVenta = 'pendiente';

  next();
}, calcularTotalVentasFacturadas, calcularTotalVentasPendientes, processVenta);

router.get("/cancelado", function (req, res, next) {

  res.locals.tabSeleccionado = 'cancelado';
  req.session.estadoVenta = 'cancelado';

  next();
}, calcularTotalVentasFacturadas, calcularTotalVentasPendientes, processVenta);

router.get("/anulado", function (req, res, next) {

  res.locals.tabSeleccionado = 'anulado';
  req.session.estadoVenta = 'anulado';

  next();
}, calcularTotalVentasFacturadas, calcularTotalVentasPendientes, processVenta);

router.get("/create", async function (req, res, next) {

  res.locals.content = 'venta/form';
  res.locals.tituloForm = "Registrar Ventas";
  res.locals.title = "Registrar Ventas";

  let filter = { where: { estado: 1 } };
  res.locals.empresas = await Models.Empresa.findAll(filter);

  filter = { where: { estado: 1 } };
  res.locals.proyectos = await Models.Proyecto.findAll(filter);

  res.locals.obj = Models.Venta.build();

  let fechaActual = moment();   // Obtener fecha actual

  fechaActual = moment(fechaActual).format('YYYY-MM-DD');

  res.locals.obj = { fechaVenta: fechaActual };
  
  res.locals.tags = config.tags;

  res.render('page');
});

router.post("/save", upload.any(), async function (req, res, next) {

  console.log("SAVE VENTA", req.body);

  let filter = { where: { idVenta: req.body.idVenta } };
  console.log("");
  console.log("FILTER: ", filter);
  const [v, created] = await Models.Venta.findOrCreate(filter);

  // si se está creando una venta...
  if (v.numeroSerieVenta == undefined) {

    filter = { where: { estado: 1, numeroSerieVenta: req.body.numeroSerieVenta } };
    let n = await Models.Venta.count(filter);

    if (n > 0) {
      console.log("DATOS INVÁLIDOS");
      req.flash('error', 'El número de factura ya existe.');
      res.redirect("/admin/venta/");
      return;
    }

    v.estado = 1;

  }

  filter = { where: { rucEmpresa: req.body.idEmpresa } };
  let empresa = await Models.Empresa.findOne(filter);

  v.numeroSerieVenta = req.body.numeroSerieVenta;
  v.idEmpresaVenta = empresa.idEmpresa/* .toString() */;
  v.idProyecto = req.body.idProyecto;
  v.tituloVenta = req.body.tituloVenta;
  v.numeroFactura = req.body.numeroFactura;
  v.descripcionVenta = req.body.descripcionVenta;

  v.totalVenta = req.body.totalVenta;
  v.igv = req.body.igv;
  v.neto = req.body.neto;

  v.fechaVenta = req.body.fechaVenta;
  v.fechaDeclaracion = req.body.fechaVenta;


  if (req.files.length > 0) {
    let file = req.files[0];

    if (file.mimetype != 'application/pdf' && file.mimetype != 'image/jpeg') {
      console.log("FILE NOT FORMAT", file.mimetype, file.originalname);
      req.flash('info', 'Formato de archivo incorrecto');
      res.redirect("/admin/venta/");
      return;
    }

    v.adjunto = file.filename;

  }

  console.log("DATOS A GUARDAR: ", v);

  await v.save();

  res.redirect('/admin/venta/');
});

router.get("/edit/:idVenta", async function (req, res, next) {

  res.locals.content = 'venta/form';
  res.locals.tituloForm = "Editar Venta";
  res.locals.title = "Editar Venta";

  let filter = { where: { estado: 1 } };
  res.locals.empresas = await Models.Empresa.findAll(filter);

  filter = { where: { estado: 1 } };
  res.locals.proyectos = await Models.Proyecto.findAll(filter);

  res.locals.obj = await Models.Venta.findByPk(req.params.idVenta);

  res.locals.tags = config.tags;

  res.locals.empresaSeleccionada = "selected";

  res.render('page');
});

router.get("/delete/:idVenta", async function (req, res, next) {

  res.locals.venta = { idVenta: req.params.idVenta };

  res.locals.content = 'venta/delete';
  res.locals.tituloForm = "Eliminar Venta";
  res.locals.title = "Eliminar Venta"

  res.locals.obj = await Models.Venta.findByPk(req.params.idVenta);

  res.render('page');
});

router.post("/delete/:idVenta", async function (req, res, next) {

  let obj = await Models.Venta.findByPk(req.params.idVenta);
  obj.estado = 0;

  await obj.save();

  res.redirect('/admin/venta/');
});

router.get("/:idVenta/detalle", async function (req, res, next) {

  res.locals.content = 'venta/detalle';

  let filter = { where: { estado: 1, idVenta: req.params.idVenta } };
  let venta = await Models.Venta.findOne(filter);

  filter = { where: { estado: 1, idProyecto: venta.idProyecto } };
  let proyecto = await Models.Proyecto.findOne(filter);

  filter = { where: { estado: 1, idEmpresa: venta.idEmpresaVenta } };
  let empresa = await Models.Empresa.findOne(filter);

  res.locals.estadoVentaActivo = venta.estadoVenta;
  res.locals.venta = venta;
  res.locals.proyecto = proyecto;
  res.locals.empresa = empresa;

  res.render("page");
});

router.param("idVenta", async function(req, res, next, id){
  console.log("PROCESANDO IDVENTA", id);

  let filter = { where: { estado: 1, idVenta: req.params.idVenta } };
  let venta = await Models.Venta.findOne(filter);

  req.venta = venta; 

  next();

});

router.get("/:idVenta/mail", async function (req, res, next) {

  res.locals.correo = "douglaszs397@hotmail.com";


  //FIXME codigo de prueba
  req.venta.tag = "tag1";

  let cuentas = [];
  
  for ( c of config.cuentasbancarias ){
    if ( c.tag == req.venta.tag ){
      cuentas.push(c);
    }
  }

  let ejs = require('ejs');
  res.locals.cuentasbancarias = cuentas;

  let html = '';

  let ejsfile = process.cwd()+'/views/venta/mail.ejs';

  ejs.renderFile(`${ejsfile}`, res.locals, {}, function(err, str)
  { 
    html = str;

    if(err){
      console.log(err);
    }
  
  /*   console.log(`__dirname`, __dirname);
    console.log(process.cwd()); */

    let mailgun = require('mailgun-js')({apiKey: config.mailgun.apikey, domain: config.mailgun.domain});

    let maildata = {
      from: 'info@nodefire.net',
      to: res.locals.correo, // req.venta.empresa.correo
      subject: `Factura ${req.venta.idVenta} `,
      text: 'Correo de prueba',
      html: html
    };

    mailgun.messages().send( maildata, function(err,body){
      console.log(err);

    });

  });
 


  

  res.render("venta/mail");
});

router.post("/:idVenta/mail", async function (req, res, next) {

});


module.exports = router;
