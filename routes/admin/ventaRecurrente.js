var express = require('express');
const Models = require('../../models');
const { Sequelize, DataTypes, Op } = require('sequelize');
var router = express.Router();
var multer = require('multer');
var upload = multer({ dest: 'public/uploads/files' });
var moment = require('moment');
moment.locale('es');
var CronJob = require('cron').CronJob;

// ==================== VENTAS RECURRENTES ====================

router.get("/", async function (req, res, next) {

  res.locals.tituloForm = "Lista de Ventas Recurrentes";
  res.locals.title = "Lista de Ventas Recurrentes";
  res.locals.content = 'ventaRecurrente/list';


  let filter = { where: { estado: 1 } };

  let ventasRecurrentes = await Models.VentaRecurrente.findAll(filter);

  res.locals.ventasRecurrentes = ventasRecurrentes;

  let empresasId = [];
  for (venta of ventasRecurrentes) {
    empresasId.push(venta.idEmpresaVentaRecurrente);
  }

  filter = { where: { estado: 1, idEmpresa: empresasId } };
  let empresas = await Models.Empresa.findAll(filter);

  let objEmpresa = {};

  for (empresa of empresas) {
    objEmpresa[empresa.idEmpresa] = empresa;
  }

  res.locals.empresa = objEmpresa;

  res.render("page");
});

router.get("/create", async function (req, res, next) {

  res.locals.content = 'ventaRecurrente/form';
  res.locals.tituloForm = "Registrar Ventas Recurrentes";
  res.locals.title = "Registrar Ventas Recurrentes";

  let filter = { where: { estado: 1 } };
  res.locals.empresas = await Models.Empresa.findAll(filter);

  filter = { where: { estado: 1 } };
  res.locals.proyectos = await Models.Proyecto.findAll(filter);

  res.locals.obj = Models.VentaRecurrente.build();

  res.render('page');
});

let processVentaRecurrente = async function() {

  console.log("");
  console.log("ENTRÓ EN PROCESS VENTA RECURRENTE...");

  console.log("");
  console.log("VENTA RECURRENTE", moment().format("YYYY-MM-DD"));

  
  let filter = { where: { estado: 1, fechaVentaRecurrente: { [Op.lt]: moment().format('YYYY-MM-DD') } } }; // Sequelize LessThan
  let ventasRecurrentes = await Models.VentaRecurrente.findAll(filter);
  
  for ( vr of ventasRecurrentes ){
    console.log("VENTA RECURRENTE: ", "ID: ",vr.idVentaRecurrente, "TÍTULO: ", vr.tituloVentaRecurrente, "FECHA: ", vr.fechaVentaRecurrente, "PERIODO REC.: ", vr.periodoRecurrencia);
    console.log("VR", vr.idEmpresaVentaRecurrente);
    let venta = Models.Venta.build(); // CREACIÓN DE UNA NUEVA VENTA CON LOS DATOS INCOMPLETOS DE LA VENTA RECURRENTE

    venta.idProyecto = vr.idProyecto;
    venta.idEmpresaVenta = vr.idEmpresaVentaRecurrente;
    venta.fechaVenta = vr.fechaVentaRecurrente;
    venta.fechaDeclaracion = venta.fechaVenta
    venta.totalVenta = vr.totalVentaRecurrente;
    venta.igv = vr.igv;
    venta.neto = vr.neto;
    venta.descripcionVenta = vr.descripcionVentaRecurrente;
    venta.estado = 1;

    await venta.save();

    let nuevaFecha = moment(vr.fechaVentaRecurrente).add(vr.periodoRecurrencia, 'month').format('YYYY-MM-DD');
    console.log("FECHA ANTERIOR: ", vr.fechaVentaRecurrente);
    console.log("NUEVA FECHA A GUARDAR: ", nuevaFecha);

    vr.fechaVentaRecurrente = nuevaFecha;
    await vr.save();

  }

}

  // 'segundos(0-59) minutos(0-59) horas(0-23) diasMes(1-31) meses(1-12) diasSemana(0-7)'
  let job = new CronJob(`0 0 0 * * *`, processVentaRecurrente, null, true, 'America/Lima'); // TAREA EJECUTADA A DIARIO A LAS 00:00:00
  job.start();


router.post("/save", upload.any(), async function (req, res, next) {

  console.log("SAVE VENTA RECURRENTE: ", req.body);

  let filter = { where: { idVentaRecurrente: req.body.idVentaRecurrente } };
  const [v, created] = await Models.VentaRecurrente.findOrCreate(filter);

  filter = { where: { rucEmpresa: req.body.idEmpresa } };
  let empresa = await Models.Empresa.findOne(filter);
  
  v.idEmpresaVentaRecurrente = empresa.idEmpresa;
  v.idProyecto = req.body.idProyecto;
  v.tituloVentaRecurrente = req.body.tituloVentaRecurrente;
  v.descripcionVentaRecurrente = req.body.descripcionVentaRecurrente;

  v.totalVentaRecurrente = req.body.totalVentaRecurrente;
  v.igv = req.body.igv;
  v.neto = req.body.neto;

  v.fechaVentaRecurrente = req.body.fechaVentaRecurrente;
  v.fechaDeclaracion = req.body.fechaVentaRecurrente;
  v.periodoRecurrencia = req.body.periodoRecurrencia;
  created && (v.estado = 1);

  
  if (req.files.length > 0) {
    let file = req.files[0];

    if (file.mimetype != 'application/pdf' && file.mimetype != 'image/jpeg') {
      console.log("FILE NOT FORMAT", file.mimetype, file.originalname);
      req.flash('info', 'Formato de archivo incorrecto');
      res.redirect("/admin/ventaRecurrente/");
      return;
    }

    v.adjunto = file.filename;

  }

  console.log("DATOS A GUARDAR: ", v);
  
  await v.save();

  res.redirect('/admin/ventaRecurrente/');
});

router.get("/edit/:idVentaRecurrente", async function (req, res, next) {

  res.locals.content = 'ventaRecurrente/form';
  res.locals.tituloForm = "Editar Venta Recurrente";
  res.locals.title = "Editar Venta Recurrente";

  let filter = { where: { estado: 1 } };
  res.locals.empresas = await Models.Empresa.findAll(filter);

  filter = { where: { estado: 1 } };
  res.locals.proyectos = await Models.Proyecto.findAll(filter);

  res.locals.obj = await Models.VentaRecurrente.findByPk(req.params.idVentaRecurrente);

  res.locals.empresaSeleccionada = "selected";

  res.render('page');
});

router.get("/delete/:idVentaRecurrente", async function (req, res, next) {

  res.locals.ventaRecurrente = { idVentaRecurrente: req.params.idVentaRecurrente };

  res.locals.content = 'ventaRecurrente/delete';
  res.locals.tituloForm = "Eliminar Venta Recurrente";
  res.locals.title = "Eliminar Venta Recurrente";

  res.locals.obj = await Models.VentaRecurrente.findByPk(req.params.idVentaRecurrente);

  res.render('page');
});

router.post("/delete/:idVentaRecurrente", async function (req, res, next) {

  let obj = await Models.VentaRecurrente.findByPk(req.params.idVentaRecurrente);
  obj.estado = 0;

  await obj.save();

  res.redirect('/admin/ventaRecurrente/');
});


module.exports = router;