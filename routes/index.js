var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {

  res.redirect("/admin/login");
});

validateSession = function (req, res, next) {
  console.log("VALIDATE SESSION", req.session.root.email);

  if (req.session.root == undefined) {
    res.redirect("/admin/login");
    return;
  }
  
  res.locals.moment = moment;
  res.locals.sessionRoot = req.session.root;
  
  next();
};

router.use("/admin", require("./admin/login"));
router.use("/admin/proyecto", validateSession, require("./admin/proyecto"));
router.use("/admin/compra", validateSession, require("./admin/compra"));
router.use("/admin/venta", validateSession, require("./admin/venta"));
router.use("/admin/ventaRecurrente", validateSession, require("./admin/ventaRecurrente"));
router.use("/admin/empresa", validateSession, require("./admin/empresa"));
router.use("/admin/stats", validateSession, require("./admin/stats"));

module.exports = router;
